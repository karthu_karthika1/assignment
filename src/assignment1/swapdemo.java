package assignment1;

import java.util.Scanner;

public class swapdemo {

	public static void main(String[] args) {
		int a,b;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a ");
		a=sc.nextInt();
		System.out.println("Enter b ");
		b=sc.nextInt();
		System.out.println("Before swap value of a= "+a);
		System.out.println("Before swap value of b= "+b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("**************************");
		System.out.println("After swap value of a = "+a);
		System.out.println("After swap value of b = "+b);
		sc.close();
		
	}

}
